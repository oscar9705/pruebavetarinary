package com.playtechnologies.demo.controller;

import com.playtechnologies.demo.model.Cliente;
import com.playtechnologies.demo.model.Credencial;
import com.playtechnologies.demo.model.dto.JwtDto;
import com.playtechnologies.demo.model.dto.Message;
import com.playtechnologies.demo.model.dto.Login;
import com.playtechnologies.demo.model.jwt.JwtProvider;
import com.playtechnologies.demo.service.ClienteService;
import com.playtechnologies.demo.service.CredencialService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@RestController
@RequestMapping("/auth")
@CrossOrigin
@Api("auth")
public class AuthController {
    @Value("${jwt.expiration}")
    private int expiration;
    private final ClienteService clienteService;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;
    private final CredencialService credencialService;
    private final JwtProvider jwtProvider;

    @Autowired
    public AuthController(ClienteService clienteService, PasswordEncoder passwordEncoder, AuthenticationManager authenticationManager, CredencialService credencialService, JwtProvider jwtProvider) {
        this.clienteService = clienteService;
        this.passwordEncoder = passwordEncoder;
        this.authenticationManager = authenticationManager;
        this.credencialService = credencialService;
        this.jwtProvider = jwtProvider;
    }

    @PostMapping("/login")
    @ApiOperation(value = "Iniciar sesión", response = Login.class)
    public ResponseEntity<JwtDto> login(@RequestBody Login login, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ResponseEntity(new Message("campos mal puestos"), HttpStatus.BAD_REQUEST);
        }

        Authentication authentication =
                authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(login.getUsername(), login.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtProvider.generateToken(authentication);

        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        JwtDto jwtDto = new JwtDto(jwt, userDetails.getUsername(),userDetails.getAuthorities());

        return new ResponseEntity(jwtDto, HttpStatus.OK);
    }
    @PostMapping(path = "/register")
    @ApiOperation(value = "Register Cliente", response = Cliente.class)
    public ResponseEntity<Cliente> saveUser(@RequestBody Cliente user) {
        String pass = passwordEncoder.encode(user.getContrasenia());
        user.setRol("Cliente");
        user.setFechaCreacion(LocalDate.now());
        user.setContrasenia(pass);
        Credencial credencial = new Credencial();
        credencial.setCorreo(user.getCorreo());
        credencial.setContrasenia(user.getContrasenia());
        credencial.setRol(user.getRol());
        user.setCredencial(credencialService.saveCredencial(credencial).get());
        return new ResponseEntity(clienteService.saveCliente(user),HttpStatus.OK);

    }


    @GetMapping(path = "/username")
    public ResponseEntity<JwtDto> getUsernameWithToken(@RequestParam(name = "token") String token) {
        JwtDto jwdto = new JwtDto(token,jwtProvider.getUsernameFromToken(token),null);
        return new ResponseEntity(jwdto, HttpStatus.OK);

    }
}

