package com.playtechnologies.demo.controller;

import com.playtechnologies.demo.exception.RequestExceptionGeneral;
import com.playtechnologies.demo.model.Cliente;
import com.playtechnologies.demo.model.Credencial;
import com.playtechnologies.demo.service.ClienteService;
import com.playtechnologies.demo.service.CredencialService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/cliente")
@Api("cliente")
public class ClienteController {
    private final ClienteService clienteService;
    private final CredencialService credencialService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public ClienteController(ClienteService clienteService, CredencialService credencialService, PasswordEncoder passwordEncoder) {
        this.clienteService = clienteService;
        this.credencialService = credencialService;
        this.passwordEncoder = passwordEncoder;
    }
    @PostMapping(path = "/save")
    @ApiOperation(value = "Crear un cliente", response = Cliente.class)
    public ResponseEntity<Cliente> saveCliente(@RequestBody Cliente cliente){
        String pass = passwordEncoder.encode(cliente.getContrasenia());
        cliente.setContrasenia(pass);
        cliente.setFechaCreacion(LocalDate.now());
        cliente.setRol("Cliente");
        Credencial credencial = new Credencial();
        credencial.setCorreo(cliente.getCorreo());
        credencial.setContrasenia(cliente.getContrasenia());
        credencial.setRol(cliente.getRol());
        cliente.setCredencial(credencial);
        credencialService.saveCredencial(credencial);
        Optional<Cliente> clienteOptional = clienteService.saveCliente(cliente);

        return ResponseEntity.ok(clienteOptional.orElseThrow(()-> new RequestExceptionGeneral("No se guardó")));
    }

    @GetMapping(path = "/all")
    @ApiOperation(value ="listar clientes",response = Cliente.class)
    public ResponseEntity<List<Cliente>> findAll(){
        return new ResponseEntity<>(clienteService.findAllCliente(), HttpStatus.OK);

    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "actualizar cliente",response = Cliente.class)
    public ResponseEntity<Cliente> updateCliente(@RequestBody Cliente cliente){
        if(clienteService.updateCliente(cliente).isPresent()) {
            Credencial credencial = new Credencial();
            credencial.setCorreo(cliente.getCorreo());
            credencial.setContrasenia(cliente.getContrasenia());
            credencial.setId(cliente.getCredencial().getId());
            credencial.setRol(cliente.getRol());
            cliente.setCredencial(credencial);
            credencialService.updateCredencial(credencial);
            return new ResponseEntity(clienteService.updateCliente(cliente).get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null,HttpStatus.BAD_REQUEST);
        }
    }


    @GetMapping(path = "/id")
    @ApiOperation(value = "cliente por id", response = Cliente.class)
    public ResponseEntity<Cliente> findById(@RequestParam(name = "id") Long id){
        return ResponseEntity.ok(clienteService.findByIdCliente(id)
                .orElseThrow(() -> new RequestExceptionGeneral("No se encontró el cliente")));
    }

    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "eliminar por id",response = Cliente.class)
    public void deleteById(@RequestParam(name="id") Long id){
        clienteService.deleteCliente(id);
    }
}
