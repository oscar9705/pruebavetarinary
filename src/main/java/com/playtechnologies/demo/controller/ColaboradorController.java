package com.playtechnologies.demo.controller;


import com.playtechnologies.demo.exception.RequestExceptionGeneral;
import com.playtechnologies.demo.model.Colaborador;
import com.playtechnologies.demo.model.Credencial;
import com.playtechnologies.demo.service.ColaboradorService;
import com.playtechnologies.demo.service.CredencialService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/colaborador")
@Api("colaborador")
public class ColaboradorController {
    private final ColaboradorService colaboradorService;
    private final CredencialService credencialService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public ColaboradorController(ColaboradorService colaboradorService, CredencialService credencialService, PasswordEncoder passwordEncoder) {
        this.colaboradorService = colaboradorService;
        this.credencialService = credencialService;
        this.passwordEncoder = passwordEncoder;
    }
    @PostMapping(path = "/save")
    @ApiOperation(value = "Crear un colaborador", response = Colaborador.class)
    public ResponseEntity<Colaborador> saveColaborador(@RequestBody Colaborador colaborador){
        String pass = passwordEncoder.encode(colaborador.getContrasenia());
        colaborador.setFechaCreacion(LocalDate.now());
        colaborador.setContrasenia(pass);
        colaborador.setRol("Colaborador");
        Credencial credencial = new Credencial();
        credencial.setCorreo(colaborador.getCorreo());
        credencial.setContrasenia(colaborador.getContrasenia());
        credencial.setRol(colaborador.getRol());
        colaborador.setCredencial(credencial);

        credencialService.saveCredencial(credencial);
        Optional<Colaborador> colaboradorOptional = colaboradorService.saveColaborador(colaborador);
        if(colaboradorOptional.isPresent())
            return new ResponseEntity<>(colaboradorOptional.get(), HttpStatus.OK);

        else
            return new ResponseEntity<>(null,HttpStatus.BAD_REQUEST);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value ="listar colaboradores",response = Colaborador.class)
    public ResponseEntity<List<Colaborador>> findAll(){
        return new ResponseEntity<>(colaboradorService.findAllColaborador(), HttpStatus.OK);

    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "actualizar colaborador",response = Colaborador.class)
    public ResponseEntity<Colaborador> updateColaborador(@RequestBody Colaborador colaborador){
        return new ResponseEntity<>(colaboradorService.updateColaborador(colaborador).get(), HttpStatus.OK);
    }

    @GetMapping(path = "/id")
    @ApiOperation(value = "colaborador por id", response = Colaborador.class)
    public ResponseEntity<Colaborador> findById(@RequestParam(name = "id") Long id){
        return ResponseEntity.ok(colaboradorService.findByIdColaborador(id)
                .orElseThrow(() -> new RequestExceptionGeneral("No se encontró el cliente")));
    }

    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "eliminar por id",response = Colaborador.class)
    public void deleteById(@RequestParam(name="id") Long id){
        colaboradorService.deleteColaborador(id);
    }
}
