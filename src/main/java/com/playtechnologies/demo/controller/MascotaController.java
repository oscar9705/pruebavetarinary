package com.playtechnologies.demo.controller;

import com.playtechnologies.demo.exception.RequestExceptionGeneral;
import com.playtechnologies.demo.model.Mascota;
import com.playtechnologies.demo.service.MascotaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/mascota")
@Api("mascota")
public class MascotaController {

    private final MascotaService mascotaService;

    @Autowired
    public MascotaController(MascotaService mascotaService) {
        this.mascotaService = mascotaService;
    }
    @PostMapping(path = "/save")
    @ApiOperation(value = "Crear una mascota", response = Mascota.class)
    public ResponseEntity<Mascota> saveMascota(@RequestBody Mascota mascota){
        mascota.setFechaCreacion(LocalDate.now());
        Optional<Mascota> MascotaOptional = mascotaService.saveMascota(mascota);
        if(MascotaOptional.isPresent())
            return new ResponseEntity<>(MascotaOptional.get(), HttpStatus.OK);

        else
            return new ResponseEntity<>(null,HttpStatus.BAD_REQUEST);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value ="listar Mascotas",response = Mascota.class)
    public ResponseEntity<List<Mascota>> findAll(){
        return new ResponseEntity<>(mascotaService.findAllMascota(), HttpStatus.OK);

    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "actualizar mascota",response = Mascota.class)
    public ResponseEntity<Mascota> updateMascota(@RequestBody Mascota mascota){

        return new ResponseEntity<>(mascotaService.updateMascota(mascota).get(), HttpStatus.OK);
    }

    @GetMapping(path = "/id")
    @ApiOperation(value = "mascota por id", response = Mascota.class)
    public ResponseEntity<Mascota> findById(@RequestParam(name = "id") Long id){
        return ResponseEntity.ok(mascotaService.findByIdMascota(id)
                .orElseThrow(() -> new RequestExceptionGeneral("No se encontró la mascota")));
    }

    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "eliminar por id",response = Mascota.class)
    public void deleteById(@RequestParam(name="id") Long id){
        mascotaService.deleteMascota(id);
    }
}
