package com.playtechnologies.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name= "clientes")
public class Cliente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "nombres")
    private String nombres;

    @Column(name = "apellidos")
    private String apellidos;

    @Column(name = "tipo_documento")
    private String tipoDocumento;

    @Column(name = "valor_documento")
    private String valorDocumento;

    @Column(name = "sexo")
    private String sexo;

    @Column(name = "correo")
    private String correo;

    @Column(name = "contrasenia")
    private String contrasenia;

    @Column(name = "departamento")
    private String departamento;

    @Column(name = "ciudad")
    private String ciudad;

    @Column(name = "direccion")
    private String direccion;

    @Column(name = "rol")
    private String rol;

    @Column(name = "estado")
    private Boolean estado;

    @Column(name = "fecha_creacion")
    private LocalDate fechaCreacion;

    @OneToOne
    @JoinColumn( name = "id_credencial", referencedColumnName = "id")
    private Credencial credencial;



    public Cliente() {
        // Constructor
    }

    public Cliente(Cliente cliente) {
        this.id = cliente.id;
        this.nombres = cliente.nombres;
        this.apellidos = cliente.apellidos;
        this.tipoDocumento = cliente.tipoDocumento;
        this.valorDocumento = cliente.valorDocumento;
        this.sexo = cliente.sexo;
        this.correo = cliente.correo;
        this.departamento = cliente.departamento;
        this.ciudad = cliente.ciudad;
        this.direccion = cliente.direccion;
        this.rol = cliente.rol;
        this.estado = cliente.estado;
        this.fechaCreacion = cliente.fechaCreacion;

    }

    public Cliente(Long id, String nombres, String apellidos, String tipoDocumento, String valorDocumento, String sexo, String correo,String contrasenia ,String departamento, String ciudad, String direccion, String rol, Boolean estado, LocalDate fechaCreacion) {
        this.id = id;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.tipoDocumento = tipoDocumento;
        this.valorDocumento = valorDocumento;
        this.sexo = sexo;
        this.correo = correo;
        this.contrasenia = contrasenia;
        this.departamento = departamento;
        this.ciudad = ciudad;
        this.direccion = direccion;
        this.rol = rol;
        this.estado = estado;
        this.fechaCreacion = fechaCreacion;

    }

    public Cliente(Long id, String nombres, String apellidos, String tipoDocumento, String valorDocumento, String sexo, String correo, String contrasenia, String departamento, String ciudad, String direccion, String rol, Boolean estado, LocalDate fechaCreacion, Credencial credencial) {
        this.id = id;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.tipoDocumento = tipoDocumento;
        this.valorDocumento = valorDocumento;
        this.sexo = sexo;
        this.correo = correo;
        this.contrasenia = contrasenia;
        this.departamento = departamento;
        this.ciudad = ciudad;
        this.direccion = direccion;
        this.rol = rol;
        this.estado = estado;
        this.fechaCreacion = fechaCreacion;
        this.credencial = credencial;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getValorDocumento() {
        return valorDocumento;
    }

    public void setValorDocumento(String valorDocumento) {
        this.valorDocumento = valorDocumento;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public LocalDate getFechaCreacion() {
        return LocalDate.now();
    }

    public void setFechaCreacion(LocalDate fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public Credencial getCredencial() {
        return credencial;
    }

    public void setCredencial(Credencial credencial) {
        this.credencial = credencial;
    }
}
