package com.playtechnologies.demo.repository;



import com.playtechnologies.demo.model.Colaborador;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface ColaboradorRepository extends JpaRepository<Colaborador,Long> {

}
