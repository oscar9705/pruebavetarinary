package com.playtechnologies.demo.repository;

import com.playtechnologies.demo.model.HistoriaClinica;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HistoriaClinicaRepository extends JpaRepository<HistoriaClinica, Long> {
}
