package com.playtechnologies.demo.service;


import com.playtechnologies.demo.model.HistoriaClinica;
import com.playtechnologies.demo.repository.HistoriaClinicaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class HistoriaClinicaService {
    private final HistoriaClinicaRepository historiaClinicaRepository;

    @Autowired
    public HistoriaClinicaService(HistoriaClinicaRepository historiaClinicaRepository) {
        this.historiaClinicaRepository = historiaClinicaRepository;
    }
    public Optional<HistoriaClinica> saveHistoriaClinica(HistoriaClinica historiaClinica){
        return Optional.of(historiaClinicaRepository.save(historiaClinica));
    }
    public Optional<HistoriaClinica> updateHistoriaClinica(HistoriaClinica historiaClinica){
        return Optional.of(historiaClinicaRepository.save(historiaClinica));
    }
    public Optional<HistoriaClinica> findByIdHistoriaClinica(Long id){
        return historiaClinicaRepository.findById(id);
    }
    public List<HistoriaClinica> findAllHistoriaClinica(){
        return historiaClinicaRepository.findAll();
    }
    public void deleteHistoriaClinica(Long id){
        historiaClinicaRepository.deleteById(id);
    }
}
