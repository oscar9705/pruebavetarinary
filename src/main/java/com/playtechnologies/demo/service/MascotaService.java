package com.playtechnologies.demo.service;

import com.playtechnologies.demo.model.Mascota;
import com.playtechnologies.demo.repository.MascotaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MascotaService {
    private final MascotaRepository mascotaRepository;

    @Autowired
    public MascotaService(MascotaRepository mascotaRepository) {
        this.mascotaRepository = mascotaRepository;
    }

    public Optional<Mascota> saveMascota(Mascota mascota){
        return Optional.of(mascotaRepository.save(mascota));
    }
    public Optional<Mascota> updateMascota(Mascota mascota ){
        return Optional.of(mascotaRepository.save(mascota));
    }
    public Optional<Mascota> findByIdMascota(Long id){
        return mascotaRepository.findById(id);
    }
    public List<Mascota> findAllMascota(){
        return mascotaRepository.findAll();
    }
    public void deleteMascota(Long id){
        mascotaRepository.deleteById(id);
    }
}
