package com.playtechnologies.demo.service;


import com.playtechnologies.demo.model.Revision;
import com.playtechnologies.demo.repository.RevisionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RevisionService {
    private final RevisionRepository revisionRepository;

    @Autowired
    public RevisionService(RevisionRepository revisionRepository) {
        this.revisionRepository = revisionRepository;
    }

    public Optional<Revision> saveRevision(Revision revision){
        return Optional.of(revisionRepository.save(revision));
    }
    public Optional<Revision> updateRevision(Revision revision){
        return Optional.of(revisionRepository.save(revision));

    }
    public Optional<Revision> findByIdRevision(Long id){
        return revisionRepository.findById(id);
    }
    public List<Revision> findAllRevision(){
        return revisionRepository.findAll();
    }
    public void deleteRevision(Long id){
        revisionRepository.deleteById(id);
    }
}
