package com.playtechnologies.demo.service;

import com.playtechnologies.demo.model.Credencial;
import com.playtechnologies.demo.model.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    CredencialService credencialService;

    @Override
    //convierte a usuario en usuarioPrincipal obtiene los datos y privilegios
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Credencial user = credencialService.findByCorreo(username).get();
        return UserDetailsImpl.build(user);
    }
}
