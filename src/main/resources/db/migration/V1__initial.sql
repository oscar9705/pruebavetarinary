/* bd
CREATE DATABASE veterinariaprueba
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;
*/

CREATE TABLE public.clientes
(
    id serial NOT NULL,
    nombres character varying(50) NOT NULL,
    apellidos character varying(50) NOT NULL,
    tipo_documento character varying(100) NOT NULL,
    valor_documento character varying(100) NOT NULL,
    sexo character varying(100) NOT NULL,
    correo character varying(100) NOT NULL,
    contrasenia character  varying (100) NOT NULL,
    departamento character varying(100) NOT NULL,
    ciudad character varying(100) NOT NULL,
    direccion character varying(100) NOT NULL,
    rol character  varying (60) NOT NULL,
    estado boolean NOT NULL,
    fecha_creacion  date NOT Null,
    id_credencial integer ,
    PRIMARY KEY (id)
);

ALTER TABLE public.clientes
    OWNER to postgres;

CREATE TABLE public.colaboradores
(
    id serial NOT NULL,
    nombres character varying(50) NOT NULL,
    apellidos character varying(50) NOT NULL,
    tipo_documento character varying(100) NOT NULL,
    valor_documento character varying(100) NOT NULL,
    sexo character varying(50) NOT NULL,
    correo character varying(100) NOT NULL,
    contrasenia character  varying (100) NOT NULL,
    departamento character varying(100) NOT NULL,
    ciudad character varying(100) NOT NULL,
    direccion character varying(100) NOT NULL,
    rol character varying (100) NOT NULL,
    tipo_colaborador character varying (70) NOT NULL,
    fecha_nacimiento date NOT NULL,
    estado boolean NOT NULL,
    fecha_creacion  date NOT Null,
    id_credencial integer ,
    PRIMARY KEY (id)
);

ALTER TABLE public.colaboradores
    OWNER to postgres;

CREATE TABLE public.credenciales
(
    id serial NOT NULL,
    correo character varying(100) NOT NULL,
    contrasenia character  varying (100) NOT NULL,
    rol character  varying (100) NOT NULL,

    PRIMARY KEY (id)
);

ALTER TABLE public.credenciales
    OWNER to postgres;

    CREATE TABLE public.mascotas
(
    id serial NOT NULL,
    nombre character varying(100) NOT NULL,
    raza character varying(100)  NOT NULL,
    sexo character varying(100) NOT NULL,
    edad integer ,
    fecha_creacion  date NOT Null,
    estado boolean NOT NULL,
    id_cliente integer ,
    PRIMARY KEY (id)
);

ALTER TABLE public.mascotas
    OWNER to postgres;

CREATE TABLE public.historias_clinicas
(
    id serial NOT NULL,
    fecha date NOT NULL,
    hora time without time zone NOT NULL,
    id_mascota integer,
    estado boolean NOT NULL,

    PRIMARY KEY (id)
);

ALTER TABLE public.historias_clinicas
    OWNER to postgres;

CREATE TABLE public.revisiones
(
    id serial NOT NULL,
    fecha date NOT NULL,
    hora time without time zone NOT NULL,
    temperatura double precision NOT NULL,
    peso double precision NOT NULL,
    frecuencia_cardiaca integer NOT NULL,
    observacion character varying(20) NOT NULL,
    id_historia_clinica integer,
    id_colaborador integer,
    estado boolean NOT NULL,

    PRIMARY KEY (id)
);

ALTER TABLE public.revisiones
    OWNER to postgres;


ALTER TABLE public.historias_clinicas
    ADD CONSTRAINT fk_mascota FOREIGN KEY (id_mascota)
    REFERENCES public.mascotas (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE
    NOT VALID;
CREATE INDEX fki_fk_mascota
    ON public.historias_clinicas(id_mascota);

ALTER TABLE public.clientes
    ADD CONSTRAINT fk_credencial_cl FOREIGN KEY (id_credencial)
    REFERENCES public.credenciales (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE
    NOT VALID;
CREATE INDEX fki_fk_credencial_cl
    ON public.clientes(id_credencial);

ALTER TABLE public.colaboradores
    ADD CONSTRAINT fk_credencial_co FOREIGN KEY (id_credencial)
    REFERENCES public.credenciales (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE
    NOT VALID;
CREATE INDEX fki_fk_credencial_co
    ON public.colaboradores(id_credencial);

ALTER TABLE public.mascotas
    ADD CONSTRAINT fk_cliente FOREIGN KEY (id_cliente)
    REFERENCES public.clientes (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE
    NOT VALID;
CREATE INDEX fki_fk_cliente
    ON public.mascotas(id_cliente);

ALTER TABLE public.revisiones
    ADD CONSTRAINT fk_historia_clinica FOREIGN KEY (id_historia_clinica)
    REFERENCES public.historias_clinicas (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE
    NOT VALID;
CREATE INDEX fki_fk_historia_clinica
    ON public.revisiones(id_historia_clinica);

ALTER TABLE public.revisiones
    ADD CONSTRAINT fk_colaborador FOREIGN KEY (id_colaborador)
    REFERENCES public.colaboradores (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE
    NOT VALID;
CREATE INDEX fki_fk_colaborador
    ON public.revisiones(id_colaborador);