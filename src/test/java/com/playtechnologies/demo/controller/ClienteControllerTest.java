package com.playtechnologies.demo.controller;

import com.playtechnologies.demo.factory.ClienteFactory;
import com.playtechnologies.demo.factory.CredencialFactory;
import com.playtechnologies.demo.model.Cliente;
import com.playtechnologies.demo.model.Credencial;
import com.playtechnologies.demo.service.ClienteService;
import com.playtechnologies.demo.service.CredencialService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


public class ClienteControllerTest {
    private Cliente cliente;
    private Optional<Cliente> clienteOptional;
    private Credencial credencial;
    private Optional<Credencial> credencialOptional;

    @Mock
    private ClienteService clienteService;

    @Mock
    private CredencialService credencialService;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private ClienteController clienteController;

    @BeforeEach
    public void init(){
        cliente = new ClienteFactory().crearEntidad();
        clienteOptional = Optional.of(cliente);
        credencial = new CredencialFactory().crearEntidad();
        credencialOptional = Optional.of(credencial);

        MockitoAnnotations.initMocks(this);
    }
    @Test
    void saveClienteTest(){
        Mockito.when(clienteService.saveCliente(cliente)).thenReturn(clienteOptional);
        Mockito.when(credencialService.saveCredencial(credencial)).thenReturn(credencialOptional);
        Mockito.when(passwordEncoder.encode(cliente.getContrasenia())).thenReturn(cliente.getContrasenia());

        ResponseEntity<Cliente> cliente1 = clienteController.saveCliente(cliente);

        assertEquals(cliente1.getStatusCodeValue(), 200);
    }
    @Test
    void updateClienteTest(){
        Mockito.when(clienteService.updateCliente(cliente)).thenReturn(clienteOptional);

        Credencial credencial = new CredencialFactory().crearEntidad();
        cliente.setCredencial(credencial);
        ResponseEntity<Cliente> cliente1 = clienteController.updateCliente(cliente);

        assertEquals(cliente1.getStatusCodeValue(), 200);
    }

    @Test
    void findByIdTest(){
        Mockito.when(clienteService.findByIdCliente(cliente.getId())).thenReturn(clienteOptional);

        ResponseEntity<Cliente> cliente1 = clienteController.findById(cliente.getId());

        assertNotNull(cliente1);
        assertEquals(cliente1.getStatusCodeValue(),200);
        assertEquals(cliente1.getBody(), cliente);

    }

    @Test
    void findByAllTest(){
        final Cliente cliente = new Cliente();

        Mockito.when(clienteService.findAllCliente()).thenReturn(Arrays.asList(cliente));

        final ResponseEntity<List<Cliente>> response = clienteController.findAll();

        assertEquals(response.getStatusCodeValue(),200);
        assertEquals(response.getBody().size(),1);
    }

    @Test
    void deleteByIdTest() {
         clienteController.deleteById(cliente.getId());
         Mockito.verify(clienteService, Mockito.times(1)).deleteCliente(cliente.getId());
    }


}
