package com.playtechnologies.demo.controller;

import com.playtechnologies.demo.factory.HistoriaClinicaFactory;
import com.playtechnologies.demo.model.HistoriaClinica;
import com.playtechnologies.demo.service.HistoriaClinicaService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class HistoriaClinicaTest {
    private HistoriaClinica historiaClinica;
    private Optional<HistoriaClinica> historiaClinicaOptional;

    @Mock
    private HistoriaClinicaService historiaClinicaService;

    @InjectMocks
    private HistoriaClinicaController historiaClinicaController;

    @BeforeEach
    public void init(){
        historiaClinica = new HistoriaClinicaFactory().crearEntidad();
        historiaClinicaOptional = Optional.of(historiaClinica);

        MockitoAnnotations.initMocks(this);
    }
    @Test
    void saveHistoriaClinicaTest(){
        Mockito.when(historiaClinicaService.saveHistoriaClinica(historiaClinica)).thenReturn(historiaClinicaOptional);

        ResponseEntity<HistoriaClinica> historiaClinica1 = historiaClinicaController.saveHistoriaClinica(historiaClinica);

        assertEquals(historiaClinica1.getStatusCodeValue(), 200);
    }
    @Test
    void updateHistoriaClinicaTest(){
        Mockito.when(historiaClinicaService.updateHistoriaClinica(historiaClinica)).thenReturn(historiaClinicaOptional);

        ResponseEntity<HistoriaClinica> historiaClinica1 = historiaClinicaController.updateHistoriaClinica(historiaClinica);

        assertEquals(historiaClinica1.getStatusCodeValue(), 200);
    }

    @Test
    void findByIdTest(){
        Mockito.when(historiaClinicaService.findByIdHistoriaClinica(historiaClinica.getId())).thenReturn(historiaClinicaOptional);

        ResponseEntity<HistoriaClinica> historiaClinica1 = historiaClinicaController.findById(historiaClinica.getId());

        assertNotNull(historiaClinica1);
        assertEquals(historiaClinica1.getStatusCodeValue(),200);
        assertEquals(historiaClinica1.getBody(), historiaClinica);

    }

    @Test
    void findByAllTest(){
        final HistoriaClinica historiaClinica = new HistoriaClinica();

        Mockito.when(historiaClinicaService.findAllHistoriaClinica()).thenReturn(Arrays.asList(historiaClinica));

        final ResponseEntity<List<HistoriaClinica>> response = historiaClinicaController.findAll();

        assertEquals(response.getStatusCodeValue(),200);
        assertEquals(response.getBody().size(),1);
    }

    @Test
    void deleteByIdTest() {
        historiaClinicaController.deleteById(historiaClinica.getId());
        Mockito.verify(historiaClinicaService, Mockito.times(1)).deleteHistoriaClinica(historiaClinica.getId());
    }

}
