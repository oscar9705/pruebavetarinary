package com.playtechnologies.demo.controller;

import com.playtechnologies.demo.factory.MascotaFactory;
import com.playtechnologies.demo.model.Mascota;
import com.playtechnologies.demo.service.MascotaService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class MascotaControllerTest {
    private Mascota mascota;
    private Optional<Mascota> mascotaOptional;

    @Mock
    private MascotaService mascotaService;

    @InjectMocks
    private MascotaController mascotaController;

    @BeforeEach
    public void init(){
        mascota = new MascotaFactory().crearEntidad();
        mascotaOptional = Optional.of(mascota);

        MockitoAnnotations.initMocks(this);
    }
    @Test
    void saveMascotaTest(){
        Mockito.when(mascotaService.saveMascota(mascota)).thenReturn(mascotaOptional);

        ResponseEntity<Mascota> mascota1 = mascotaController.saveMascota(mascota);

        assertEquals(mascota1.getStatusCodeValue(), 200);
    }
    @Test
    void updateMascotaTest(){
        Mockito.when(mascotaService.updateMascota(mascota)).thenReturn(mascotaOptional);

        ResponseEntity<Mascota> mascota1 = mascotaController.updateMascota(mascota);

        assertEquals(mascota1.getStatusCodeValue(), 200);
    }

    @Test
    void findByIdTest(){
        Mockito.when(mascotaService.findByIdMascota(mascota.getId())).thenReturn(mascotaOptional);

        ResponseEntity<Mascota> mascota1 = mascotaController.findById(mascota.getId());

        assertNotNull(mascota1);
        assertEquals(mascota1.getStatusCodeValue(),200);
        assertEquals(mascota1.getBody(), mascota);

    }

    @Test
    void findByAllTest(){
        final Mascota mascota = new Mascota();

        Mockito.when(mascotaService.findAllMascota()).thenReturn(Arrays.asList(mascota));

        final ResponseEntity<List<Mascota>> response = mascotaController.findAll();

        assertEquals(response.getStatusCodeValue(),200);
        assertEquals(response.getBody().size(),1);
    }

    @Test
    void deleteByIdTest() {
        mascotaController.deleteById(mascota.getId());
        Mockito.verify(mascotaService, Mockito.times(1)).deleteMascota(mascota.getId());
    }

}
