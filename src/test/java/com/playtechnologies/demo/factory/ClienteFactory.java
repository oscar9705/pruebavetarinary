package com.playtechnologies.demo.factory;
import com.playtechnologies.demo.model.Cliente;

import java.time.LocalDate;
import java.util.UUID;
import java.util.Random;


public class ClienteFactory {
    private Long id;
    private String nombres;
    private String apellidos;
    private String tipoDocumento;
    private String valorDocumento;
    private String sexo;
    private String correo;
    private String contrasenia;
    private String departamento;
    private String ciudad;
    private String direccion;
    private String rol;
    private LocalDate fechaCreacion;
    private Boolean estado;


    public ClienteFactory() {
        Random random = new Random();
        LocalDate f1 =  LocalDate.of(random.nextInt(40)+2021,random.nextInt(12)+1, random.nextInt(28)+1);

        id = random.nextLong();
        nombres = UUID.randomUUID().toString();
        apellidos = UUID.randomUUID().toString();
        tipoDocumento = UUID.randomUUID().toString();
        valorDocumento = UUID.randomUUID().toString();
        sexo = UUID.randomUUID().toString();
        correo = UUID.randomUUID().toString();
        contrasenia = UUID.randomUUID().toString();
        departamento = UUID.randomUUID().toString();
        ciudad = UUID.randomUUID().toString();
        direccion = UUID.randomUUID().toString();
        rol = UUID.randomUUID().toString();
        fechaCreacion = f1;
        estado = true;

    }
    public Cliente crearEntidad(){
        return  new Cliente(id,nombres, apellidos,tipoDocumento, valorDocumento, sexo, correo, contrasenia ,departamento, ciudad, direccion, rol , estado,fechaCreacion);
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public void setValorDocumento(String valorDocumento) {
        this.valorDocumento = valorDocumento;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

}
