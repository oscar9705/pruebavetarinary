package com.playtechnologies.demo.factory;

import com.playtechnologies.demo.model.Cliente;
import com.playtechnologies.demo.model.Colaborador;

import java.time.LocalDate;
import java.util.Random;
import java.util.UUID;

public class ColaboradorFactory {
    private Long id;
    private String nombres;
    private String apellidos;
    private String tipoDocumento;
    private String valorDocumento;
    private String sexo;
    private String correo;
    private String contrasenia;
    private String departamento;
    private String ciudad;
    private String direccion;
    private String rol;
    private String tipoColaborador;
    private Boolean estado;
    private LocalDate fechaCreacion;

    public ColaboradorFactory() {
        Random random = new Random();
        LocalDate f1 =  LocalDate.of(random.nextInt(40)+2021,random.nextInt(12)+1, random.nextInt(28)+1);

        id = random.nextLong();
        nombres = UUID.randomUUID().toString();
        apellidos = UUID.randomUUID().toString();
        tipoDocumento = UUID.randomUUID().toString();
        valorDocumento = UUID.randomUUID().toString();
        sexo = UUID.randomUUID().toString();
        correo = UUID.randomUUID().toString();
        contrasenia = UUID.randomUUID().toString();
        departamento = UUID.randomUUID().toString();
        ciudad = UUID.randomUUID().toString();
        direccion = UUID.randomUUID().toString();
        rol = UUID.randomUUID().toString();
        tipoColaborador = UUID.randomUUID().toString();
        estado = true;
        fechaCreacion = f1;
    }
    public Colaborador crearEntidad(){
        return  new Colaborador(id,nombres, apellidos,tipoDocumento, valorDocumento, sexo, correo,contrasenia ,departamento, ciudad, direccion, rol , tipoColaborador ,estado, fechaCreacion);
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public void setValorDocumento(String valorDocumento) {
        this.valorDocumento = valorDocumento;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public void setTipoColaborador(String tipoColaborador) {
        this.tipoColaborador = tipoColaborador;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
}
