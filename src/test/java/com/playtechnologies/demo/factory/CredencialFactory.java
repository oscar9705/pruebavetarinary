package com.playtechnologies.demo.factory;

import com.playtechnologies.demo.model.Credencial;

import java.util.Random;
import java.util.UUID;

public class CredencialFactory {
    private Long id;
    private String correo;
    private String contrasenia;
    private String rol;

    public CredencialFactory(){
        Random random = new Random();

        id = random.nextLong();
        correo = UUID.randomUUID().toString();
        contrasenia = UUID.randomUUID().toString();
        rol = UUID.randomUUID().toString();
    }
    public Credencial crearEntidad(){
        return new Credencial(id,correo,contrasenia,rol);
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }
}
