package com.playtechnologies.demo.factory;

import com.playtechnologies.demo.model.HistoriaClinica;
import com.playtechnologies.demo.model.Mascota;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Random;

public class HistoriaClinicaFactory {
    private Long id;
    private LocalDate fecha;
    private LocalTime hora;
    private Boolean estado;
    private Mascota mascota;

    public HistoriaClinicaFactory() {
        Random random = new Random();
        LocalDate f1 =  LocalDate.of(random.nextInt(40)+2021,random.nextInt(12)+1, random.nextInt(28)+1);
        LocalTime h1 = LocalTime.of(random.nextInt(23)+1,random.nextInt(59)+1 );
        MascotaFactory mascotaFactory = new MascotaFactory();
        mascota = mascotaFactory.crearEntidad();
        id = random.nextLong();
        fecha = f1;
        hora = h1;
        estado = true;

    }
    public HistoriaClinica crearEntidad(){
        return new HistoriaClinica(id, fecha, hora, estado, mascota);
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public void setHora(LocalTime hora) {
        this.hora = hora;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
}
