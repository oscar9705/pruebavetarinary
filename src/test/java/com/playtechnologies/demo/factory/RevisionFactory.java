package com.playtechnologies.demo.factory;


import com.playtechnologies.demo.model.Revision;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Random;
import java.util.UUID;

public class RevisionFactory {
    private Long id;
    private LocalDate fecha;
    private LocalTime hora;
    private Float temperatura;
    private Float peso;
    private Integer frecuenciaCardiaca;
    private String observacion;
    private Boolean estado;

    public RevisionFactory(){
        Random random = new Random();
        LocalDate f1 =  LocalDate.of(random.nextInt(40)+2021,random.nextInt(12)+1, random.nextInt(28)+1);
        LocalTime h1 = LocalTime.of(random.nextInt(23)+1,random.nextInt(59)+1 );

        id = random.nextLong();
        fecha = f1;
        hora = h1;
        temperatura = random.nextFloat();
        peso = random.nextFloat();
        frecuenciaCardiaca = random.nextInt();
        observacion = UUID.randomUUID().toString();
        estado = true;
    }

    public Revision crearEntidad(){
        return new Revision(id, fecha, hora, temperatura, peso, frecuenciaCardiaca, observacion, estado, null, null);
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public void setHora(LocalTime hora) {
        this.hora = hora;
    }

    public void setTemperatura(Float temperatura) {
        this.temperatura = temperatura;
    }

    public void setPeso(Float peso) {
        this.peso = peso;
    }

    public void setFrecuenciaCardiaca(Integer frecuenciaCardiaca) {
        this.frecuenciaCardiaca = frecuenciaCardiaca;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
}
