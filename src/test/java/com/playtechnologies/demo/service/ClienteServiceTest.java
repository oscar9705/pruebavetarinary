package com.playtechnologies.demo.service;

import com.playtechnologies.demo.factory.ClienteFactory;
import com.playtechnologies.demo.factory.CredencialFactory;
import com.playtechnologies.demo.model.Cliente;
import com.playtechnologies.demo.model.Credencial;
import com.playtechnologies.demo.repository.ClienteRepository;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;



public class ClienteServiceTest {
    private Cliente cliente;
    private Optional<Cliente> clienteOptional;


    @Mock
    ClienteRepository clienteRepository;

    @InjectMocks
    ClienteService clienteService;

    @BeforeEach
    public void init(){
        cliente = new ClienteFactory().crearEntidad();
        clienteOptional = Optional.of(cliente);

        MockitoAnnotations.initMocks(this);
    }
    @Test
    void saveClienteTest() {
        Mockito.when(clienteRepository.save(cliente)).thenReturn(cliente);


        Optional<Cliente> clienteSave = clienteService.saveCliente(cliente);

        assertEquals(clienteSave,Optional.of(cliente));
    }

    @Test
    void findByIdExitosoTest() {
        Mockito.when(clienteRepository.findById(cliente.getId())).thenReturn(clienteOptional);

        Optional<Cliente> cliente1 = clienteService.findByIdCliente(cliente.getId());
        assertEquals(cliente1, clienteOptional);
    }
    @Test
    void findByIdFallidoTest() {
        Mockito.when(clienteRepository.findById(cliente.getId())).thenReturn(null);

        Optional<Cliente> cliente1 = clienteService.findByIdCliente(cliente.getId());
        assertNull(cliente1);
    }
    @Test
    void updateClienteTest() {
        Mockito.when(clienteRepository.save(cliente)).thenReturn(cliente);

        Optional<Cliente> clienteUpdate = clienteService.updateCliente(cliente);

        assertNotNull(clienteUpdate);
        assertEquals(clienteUpdate,Optional.of(cliente));
    }

    @Test
    void findAllTest() {
        final Cliente cliente = new Cliente();

        Mockito.when(clienteRepository.findAll()).thenReturn(Arrays.asList(cliente));

        final List<Cliente> list = clienteService.findAllCliente();

        assertNotNull(list);
        assertFalse(list.isEmpty());
        assertEquals(list.size(), 1);
    }
    @Test
    void deleteClienteTest() {

        clienteService.deleteCliente(clienteOptional.get().getId());
        Mockito.verify(clienteRepository, Mockito.times(1)).deleteById(cliente.getId());


    }
    /*@Test
    void deleteCLienteVoidTest(){
        Mockito.doNothing().when(clienteRepository.deleteById(cliente.getId()));
    }*/
}
