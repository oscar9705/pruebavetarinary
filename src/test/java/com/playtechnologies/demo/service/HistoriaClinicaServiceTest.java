package com.playtechnologies.demo.service;

import com.playtechnologies.demo.factory.HistoriaClinicaFactory;
import com.playtechnologies.demo.model.Cliente;
import com.playtechnologies.demo.model.HistoriaClinica;
import com.playtechnologies.demo.repository.HistoriaClinicaRepository;
import org.hibernate.dialect.identity.HANAIdentityColumnSupport;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class HistoriaClinicaServiceTest {
    private HistoriaClinica historiaClinica;
    private Optional<HistoriaClinica> historiaClinicaOptional;

    @Mock
    HistoriaClinicaRepository historiaClinicaRepository;

    @InjectMocks
    HistoriaClinicaService historiaClinicaService;

    @BeforeEach
    public void init(){
        historiaClinica = new HistoriaClinicaFactory().crearEntidad();
        historiaClinicaOptional = Optional.of(historiaClinica);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void saveHistoriaClinicaTest() {
        Mockito.when(historiaClinicaRepository.save(historiaClinica)).thenReturn(historiaClinica);
        Optional<HistoriaClinica> hcSave = historiaClinicaService.saveHistoriaClinica(historiaClinica);

        assertEquals(hcSave,Optional.of(historiaClinica));
    }

    @Test
    void findByIdExitosoTest() {
        Mockito.when(historiaClinicaRepository.findById(historiaClinica.getId())).thenReturn(historiaClinicaOptional);

        Optional<HistoriaClinica> HistoriaClinica1 = historiaClinicaService.findByIdHistoriaClinica(historiaClinica.getId());
        assertEquals(HistoriaClinica1, historiaClinicaOptional);
    }
    @Test
    void findByIdFallidoTest() {
        Mockito.when(historiaClinicaRepository.findById(historiaClinica.getId())).thenReturn(null);

        Optional<HistoriaClinica> HistoriaClinica1 = historiaClinicaService.findByIdHistoriaClinica(historiaClinica.getId());

        assertNull(HistoriaClinica1);
    }
    @Test
    void updateHistoriaClinicaTest() {
        Mockito.when(historiaClinicaRepository.save(historiaClinica)).thenReturn(historiaClinica);
        Optional<HistoriaClinica> hcUpdate = historiaClinicaService.updateHistoriaClinica(historiaClinica);

        assertNotNull(hcUpdate);
        assertEquals(hcUpdate,Optional.of(historiaClinica));
    }

    @Test
    void findAllTest() {
        final HistoriaClinica hc = new HistoriaClinica();

        Mockito.when(historiaClinicaRepository.findAll()).thenReturn(Arrays.asList(hc));

        final List<HistoriaClinica> list = historiaClinicaService.findAllHistoriaClinica();

        assertNotNull(list);
        assertFalse(list.isEmpty());
        assertEquals(list.size(), 1);
    }
    @Test
    void deleteHistoriaClinicaTest() {

        historiaClinicaService.deleteHistoriaClinica(historiaClinica.getId());
        Mockito.verify(historiaClinicaRepository, Mockito.times(1)).deleteById(historiaClinica.getId());


    }
}
