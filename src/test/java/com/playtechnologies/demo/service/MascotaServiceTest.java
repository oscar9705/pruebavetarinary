package com.playtechnologies.demo.service;

import com.playtechnologies.demo.factory.MascotaFactory;
import com.playtechnologies.demo.model.Mascota;
import com.playtechnologies.demo.repository.MascotaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class MascotaServiceTest {

    private Mascota mascota;
    private Optional<Mascota> mascotaOptional;

    @Mock
    MascotaRepository mascotaRepository;

    @InjectMocks
    MascotaService mascotaService;

    @BeforeEach
    public void init(){
        mascota = new MascotaFactory().crearEntidad();
        mascotaOptional = Optional.of(mascota);
        MockitoAnnotations.initMocks(this);
    }
    @Test
    void saveMascotaTest() {
        Mockito.when(mascotaRepository.save(mascota)).thenReturn(mascota);
        Optional<Mascota> mascotaSave = mascotaService.saveMascota(mascota);

        assertEquals(mascotaSave,Optional.of(mascota));
    }

    @Test
    void findByIdExitosoTest() {
        Mockito.when(mascotaRepository.findById(mascota.getId())).thenReturn(mascotaOptional);

        Optional<Mascota> mascota1 = mascotaService.findByIdMascota(mascota.getId());
        assertEquals(mascota1, mascotaOptional);
    }
    @Test
    void findByIdFallidoTest() {
        Mockito.when(mascotaRepository.findById(mascota.getId())).thenReturn(null);

        Optional<Mascota> mascota1 = mascotaService.findByIdMascota(mascota.getId());
        assertNull(mascota1);
    }
    @Test
    void updateMascotaTest() {
        Mockito.when(mascotaRepository.save(mascota)).thenReturn(mascota);

        Optional<Mascota> mascotaUpdate = mascotaService.updateMascota(mascota);

        assertNotNull(mascotaUpdate);
        assertEquals(mascotaUpdate,Optional.of(mascota));
    }

    @Test
    void findAllTest() {
        final Mascota mascota = new Mascota();

        Mockito.when(mascotaRepository.findAll()).thenReturn(Arrays.asList(mascota));

        final List<Mascota> list = mascotaService.findAllMascota();

        assertNotNull(list);
        assertFalse(list.isEmpty());
        assertEquals(list.size(), 1);
    }
    @Test
    void deleteMascotaTest() {

        mascotaService.deleteMascota(mascota.getId());
        Mockito.verify(mascotaRepository, Mockito.times(1)).deleteById(mascota.getId());


    }
}
