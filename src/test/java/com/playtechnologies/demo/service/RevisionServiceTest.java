package com.playtechnologies.demo.service;

import com.playtechnologies.demo.factory.RevisionFactory;
import com.playtechnologies.demo.model.Revision;
import com.playtechnologies.demo.repository.RevisionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class RevisionServiceTest {
    private Revision revision;
    private Optional<Revision> revisionOptional;

    @Mock
    private RevisionRepository revisionRepository;

    @InjectMocks
    private RevisionService revisionService;

    @BeforeEach
    public void init(){
        revision = new RevisionFactory().crearEntidad();
        revisionOptional = Optional.of(revision);
        MockitoAnnotations.initMocks(this);
    }
    @Test
    void saveRevisionTest() {
        Mockito.when(revisionRepository.save(revision)).thenReturn(revision);
        Optional<Revision> revisionSave = revisionService.saveRevision(revision);

        assertEquals(revisionSave,Optional.of(revision));
    }

    @Test
    void findByIdExitosoTest() {
        Mockito.when(revisionRepository.findById(revision.getId())).thenReturn(revisionOptional);

        Optional<Revision> revision1 = revisionService.findByIdRevision(revision.getId());
        assertEquals(revision1, revisionOptional);
    }
    @Test
    void findByIdFallidoTest() {
        Mockito.when(revisionRepository.findById(revision.getId())).thenReturn(null);

        Optional<Revision> revision1 = revisionService.findByIdRevision(revision.getId());
        assertNull(revision1);
    }
    @Test
    void updateRevisionTest() {
        Mockito.when(revisionRepository.save(revision)).thenReturn(revision);

        Optional<Revision> revisionUpdate = revisionService.updateRevision(revision);

        assertNotNull(revisionUpdate);
        assertEquals(revisionUpdate,Optional.of(revision));
    }

    @Test
    void findAllTest() {
        final Revision revision = new Revision();

        Mockito.when(revisionRepository.findAll()).thenReturn(Arrays.asList(revision));

        final List<Revision> list = revisionService.findAllRevision();

        assertNotNull(list);
        assertFalse(list.isEmpty());
        assertEquals(list.size(), 1);
    }
    @Test
    void deleterevisionTest() {

        revisionService.deleteRevision(revisionOptional.get().getId());
        Mockito.verify(revisionRepository, Mockito.times(1)).deleteById(revision.getId());


    }
}
